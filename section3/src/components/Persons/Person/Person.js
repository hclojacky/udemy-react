import React, { Component } from 'react'
import PropTypes from 'prop-types'

import classes from './Person.css'
import withClass from '../../../hoc/withClass';
import Aux from '../../../hoc/Aux';
import AuthContext from '../../../context/auth-context';

class Person extends Component {
  constructor(props) {
    super(props);
    this.inputElementRef = React.createRef();
  }

  static contextType = AuthContext;

  componentDidMount() {
    this.inputElementRef.current.focus();
    console.log(this.context.authenticated);
  }

  render() {
    console.log('[Person.js] rendering...')
    console.log(this.props.isAuth);
    return <Aux>     
      {this.context.authenticated ? <p>Authenticated!</p> : <p>Please log in</p>}
      <p key="i1" onClick={this.props.click}>I'm a {this.props.name} and I am {this.props.age} years old!</p>
      <p key="i2">{this.props.children}</p>
      <input 
        key="i3"
        type="text"
        onChange={this.props.change}
        value={this.props.name}
        ref={this.inputElementRef}/>
    </Aux>
  }
}

Person.propTypes = {
  click: PropTypes.func,
  name: PropTypes.string,
  age: PropTypes.number,
  changed: PropTypes.func
}

export default withClass(Person, classes.Person);