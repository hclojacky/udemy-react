import React from 'react'

const Validation = (props) => {
  return (
    <p>Message: {props.input.length < 5 ? 'Text too short' : props.input.length > 20 ? 'Text too long' : 'OK'}</p>
  )
}

export default Validation;